# Docker registry

The purpose of this project is to create a functional Docker registry server in pure-Rust.

## Authorization
Without accounts specified in config, no authentication is required to read / write
operations on registry.

With at least one account specified, a valid account is required to push images
on registry, and the registry is accessible in read-only to un-authenticated users.


Docker Registry API official specs: https://docs.docker.com/registry/spec/api

## Compilation
With Rust installed, run
```bash
cargo build --release
```

## Installation
Run the following command to initialize configuration
```shell
docker_registry init-config [conf_path]
```

To enable authentication, add credentials:
```shell
docker_registry add_user [conf_path]
```

To start in server mode:
```shell
docker_registry serve [conf_path]
```

## Work progress
- [x] API routes
    - [x] GET -  `/v2/`
    - [x] GET -  `/v2/<name>/tags/list`
    - [x] GET -  `/v2/<name>/manifests/<reference>`
    - [x] PUT -  `/v2/<name>/manifests/<reference>`
    - [x] DELETE -  `/v2/<name>/manifests/<reference>`
    - [x] GET -  `/v2/<name>/blobs/<digest>`
    - [x] DELETE -  `/v2/<name>/blobs/<digest>` => method is not supported
    - [x] POST -  `/v2/<name>/blobs/uploads/`
    - [x] GET -  `/v2/<name>/blobs/uploads/<uuid>`
    - [x] PATCH -  `/v2/<name>/blobs/uploads/<uuid>`
    - [x] PUT -  `/v2/<name>/blobs/uploads/<uuid>`
    - [x] DELETE -  `/v2/<name>/blobs/uploads/<uuid>`
    - [x] GET -  `/v2/_catalog`
- [x] Authentication support
- [ ] Basic web frontend
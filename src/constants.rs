/// JWT auth token lifetime
pub const AUTH_TOKENS_DURATION: u64 = 300;

/// Blobs uploads location
pub const UPLOAD_BLOBS_LOCATION: &str = "_uploads";

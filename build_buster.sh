#!/bin/bash

docker run --rm \
  -u 1000:1000 \
  -v $PWD:/app \
  -v /etc/ssl:/etc/ssl:ro \
  -w /app \
  -v ~/.cargo/config:/.cargo/config:ro \
  rust:slim-buster \
  cargo build --release
